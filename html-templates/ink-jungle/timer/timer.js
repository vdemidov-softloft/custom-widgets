const day = document.querySelector('#day');
const hours = document.querySelector('#hours');
const minuts = document.querySelector('#minuts');
const seconds = document.querySelector('#seconds');
const modul = document.querySelector('#timer');
const close = document.querySelector('.close-icon');
const checkTime = (i) => {
    if (i < 10) {
        i = `0${i}`;
    }
    return i;
};
close.onclick = () => {
    modul.classList.add('none');
};
const startTime = () => {
    const today = new Date().getTime();
    const endData = new Date('Oct 23, 2021 14:10:25').getTime();
    const sek = endData - today;
    if (endData <= today) {
        modul.classList.add('none');
    }
    let d = Math.floor(sek / (1000 * 60 * 60 * 24));
    let h = Math.floor((sek % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let m = Math.floor((sek % (1000 * 60 * 60)) / (1000 * 60));
    let s = Math.floor((sek % (1000 * 60)) / 1000);
    m = String(checkTime(m)).split('');
    s = String(checkTime(s)).split('');
    h = String(checkTime(h)).split('');
    d = String(checkTime(d)).split('');
    day.innerHTML = `<div class='cell-wrap'><div class='cell-day'><div class='cell cell-day-first'>${d[0]}</div><div class='cell cell-day-second'>${d[1]}</div></div><div class='cell-title'>Days</div>`;
    hours.innerHTML = `<div class='cell-wrap'><div class='cell-hours'><div class='cell cell-hours-first'>${h[0]}</div><div class='cell cell-hours-second'>${h[1]}</div></div><div class='cell-title'>Hours</div>`;
    minuts.innerHTML = `<div class='cell-wrap'><div class='cell-minuts'><div class='cell cell-minuts-first'>${m[0]}</div><div class='cell cell-minuts-second'>${m[1]}</div></div><div class='cell-title'>Minutes</div>`;
    seconds.innerHTML = `<div class='cell-wrap'><div class='cell-seconds'><div class='cell cell-seconds-first'>${s[0]}</div><div class='cell cell-seconds-second'>${s[1]}</div></div><div class='cell-title'>Seconds</div>`;
};
if (window.location.pathname === '/') {
    setInterval(startTime, 1000);
} else {
    modul.classList.add('none');
}